package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gogoapps/url-collector/internal"
	"gogoapps/url-collector/service"
	"log"
	"net/http"
)

type App struct {
	router *mux.Router
}

func (s *App) initService() {

	s.router = mux.NewRouter()
}

func main() {
	app := App{}
	app.initService()

	app.router.Handle("/pictures", service.ImageHandler{
		ImageReceiver: &service.NASAImage{},
	})

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", internal.Conf.Port), app.router))
}
