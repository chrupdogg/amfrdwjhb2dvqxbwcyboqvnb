package service

import (
	"encoding/json"
	"gogoapps/url-collector/internal"
	"gogoapps/url-collector/models"
	"net/http"
)

//ImageHandler implements the ImageReceiver interface to allow usage of independent types
type ImageHandler struct {
	ImageReceiver ImageReceiver
}

//Semaphore channel for max concurrent API requests
var semaphore = make(chan struct{}, internal.Conf.MaxConRequests)

func (i ImageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var encoder = json.NewEncoder(w)

	w.Header().Set("Content-Type", "application/json")

	if err = i.ImageReceiver.ValidateRequest(r); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	//Blocks requests if semaphore is full
	semaphore <- struct{}{}
	var imageURLs []models.ImageUrl

	imageURLs, err = i.ImageReceiver.Receive()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		encoder.Encode(models.ErrorResponse{
			Error: err.Error(),
		})
		<-semaphore
		return
	}

	encoder.Encode(models.PictureURLSResponse{Urls: imageURLs})
	//Frees up space in the semaphore when the request is finished
	<-semaphore
}
