package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"gogoapps/url-collector/internal"
	"gogoapps/url-collector/models"
	"net/http"
)

const ApiEndpoint = "https://api.nasa.gov/planetary/apod"

//NASAImage a provider for the NASA APOD images API, implements the ImageReceiver interface
type NASAImage struct {
	models.NASAImageRequest
}

func (n *NASAImage) ValidateRequest(r *http.Request) error {
	var err error
	params := r.URL.Query()

	dates := models.QueryDates{
		StartDate: params.Get("start_date"),
		EndData:   params.Get("end_date"),
	}
	if err = dates.Validate(); err != nil {
		return err
	}
	n.QueryDates = dates

	return nil
}

func (n NASAImage) Receive() ([]models.ImageUrl, error) {
	var err error
	var urls []models.ImageUrl
	var response []models.NASAImageResponse

	r, err := http.Get(ApiEndpoint + fmt.Sprintf("?api_key=%s&start_date=%s&end_date=%s", internal.Conf.ApiKey, n.QueryDates.StartDate, n.QueryDates.EndData))
	if err != nil {
		return nil, err
	}

	if r.StatusCode == http.StatusOK {
		err = json.NewDecoder(r.Body).Decode(&response)
		if err != nil {
			return urls, err
		}

		for _, v := range response {
			urls = append(urls, models.ImageUrl(v.Url))
		}

		return urls, nil
	}

	if r.StatusCode >= 400 && r.StatusCode <= 599 {
		var nasaError models.NASAErrorResponse
		err = json.NewDecoder(r.Body).Decode(&nasaError)

		return nil, errors.New(nasaError.Error.Code + ": " + nasaError.Error.Message)
	}

	return nil, err
}
