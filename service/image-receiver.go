package service

import (
	"gogoapps/url-collector/models"
	"net/http"
)

//ImageReceiver is an interface that defines the methods for receiving and validating request for a given image provider
type ImageReceiver interface {
	Receive() ([]models.ImageUrl, error)
	ValidateRequest(*http.Request) error
}
