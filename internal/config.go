package internal

import (
	"os"
	"strconv"
)

type Configuration struct {
	ApiKey         string
	MaxConRequests int
	Port           string
}

var (
	Conf Configuration
)

func init() {
	var setEnv = func(key, fallback string) string {
		if value, ok := os.LookupEnv(key); ok {
			return value
		}
		return fallback
	}
	Conf.ApiKey = setEnv("API_KEY", "DEMO_KEY")
	Conf.MaxConRequests, _ = strconv.Atoi(setEnv("CONCURRENT_REQUESTS", "5"))
	Conf.Port = setEnv("PORT", "8080")
}
