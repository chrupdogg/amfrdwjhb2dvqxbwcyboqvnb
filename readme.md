# URL-Collector - GogoApps recruitment Exercise

This repository contains the recruitment for gogoapps company.

## Description

The goal was to build a *url-collector* microservice that connects to the NASA API.
It accepts two query parameters start_date and end_date.
validates them and makes the correct request to the external API. It returns a JSON with an array of URLS to images.

## Key features

- Query params validation
- Max concurrent requests limiting 

## Running the service

To run the service first build the image

`docker build -t <TAG> .`

then run the container with exposed port

`docker run -p <port>:<internalPort> <TAG>`

## ENV Variables

The application uses three environment variables at runtime by default:
- API_KEY (default: DEMO_KEY)
- PORT (default: 8080)
- CONCURRENT_REQUESTS (default: 5)

To use different settings when running the container use `docker run --e KEY=value <IMAGE>`
To use a file created from the example.env use `docker run --env-file=./.env`

## Discussion

To reference the discussion questions that appeared in the task outline.

**What if we were to change the NASA API to some other images provider?**

Because of the usage of the *ImageReceiver* interface, the new image provider would have to implement it. When that is achieved it would be as simple as
changing the object signature in the "/picture" handler.

**What if, apart from using NASA API, we would want to have another microservice fetching urls from
European Space Agency. How much code could be reused?**

Most of the code could be reused except the NASA API implementation.

**What if we wanted to add some more query params to narrow down lists of urls - for example,
selecting only images taken by certain person. (field copyright in the API response)**

It would require adding an extra field to the NASAImageRequest struct, validate the value and assign it in the validate function and then pass it accordingly in the NASA API call.
