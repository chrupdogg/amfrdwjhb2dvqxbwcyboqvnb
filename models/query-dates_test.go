package models

import (
	"errors"
	"fmt"
	"testing"
)

var datesFormatTable = []struct {
	startDate string
	endDate   string
	error     error
}{
	{"", "", errors.New(QueryBothMissing)},
	{"", "2020-02-23", errors.New(QueryStartMissing)},
	{"2020-02-23", "", errors.New(QueryEndMissing)},
	{"2-01-20", "2020-02-23", errors.New(FormatError)},
	{"20-01-20", "2020-02-23", errors.New(FormatError)},
	{"202-01-20", "2020-02-23", errors.New(FormatError)},
	{"2022-0-20", "20-02-23", errors.New(FormatError)},
	{"2020-12-3", "2020-02-23", errors.New(FormatError)},
	{"2020-2-8", "2020-02-23", errors.New(FormatError)},
	{"2020-02-8", "2020-02-23", errors.New(FormatError)},
	{"test", "test", errors.New(FormatError)},
	{"2022-02-08", "2020-02-23", errors.New(OutOffRangeError)},
	{"2021-13-08", "2020-02-23", errors.New(OutOffRangeError)},
	{"2021-12-32", "2020-02-23", errors.New(OutOffRangeError)},
	{"2021-12-32", "2020-02-23", errors.New(OutOffRangeError)},
	{"2019-02-29", "2020-02-23", errors.New(OutOffRangeError)},
	{"2019-05-31", "2020-02-23", errors.New(OutOffRangeError)},
	{"2020-05-31", "2020-05-31", nil},
	{"2019-06-30", "2020-02-23", nil},
	{"2020-02-29", "2020-03-10", nil},
	{"2020-01-01", "2020-12-31", nil},
	{"2021-01-29", "2020-02-23", errors.New(WrongDateOrder)},
	{"2007-01-29", "2006-02-23", errors.New(WrongDateOrder)},
	{"2006-01-29", "2007-02-23", nil},
}

func TestQueryDates_Validate(t *testing.T) {

	for _, tt := range datesFormatTable {
		t.Run(fmt.Sprintf("start_date: %s, end_date: %s", tt.startDate, tt.endDate), func(t *testing.T) {
			queryDate := QueryDates{
				StartDate: tt.startDate,
				EndData:   tt.endDate,
			}
			err := queryDate.Validate()

			if tt.error == nil && err != nil {
				t.Errorf("the expected error was nil. got %v", err.Error())
			} else if err != nil {
				if err.Error() != tt.error.Error() {
					t.Errorf("data validation failed for: \n "+
						"start_date: %s \n "+
						"end_date: %s \n "+
						"error should be: %s \n"+
						"error was: %s ", tt.startDate, tt.endDate, tt.error, err)
				}
			}
		})
	}
}
