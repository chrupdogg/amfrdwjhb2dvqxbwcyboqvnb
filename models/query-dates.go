package models

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

const (
	QueryBothMissing  = "missing 'start_date' and 'end_date' parameters"
	QueryStartMissing = "missing 'start_date' parameter"
	QueryEndMissing   = "missing 'end_date' parameter"
	FormatError       = "format error, please use YYYY-MM-DD"
	OutOffRangeError  = "date out of range"
	WrongDateOrder    = "dates wrong order 'start_date' should be before 'end_date'"
)

//QueryDates defines the structure of the request dates for the range of images
type QueryDates struct {
	StartDate string
	EndData   string
}

func (n QueryDates) Validate() error {

	if n.StartDate == "" && n.EndData == "" {
		return errors.New(QueryBothMissing)
	}

	if n.StartDate == "" {
		return errors.New(QueryStartMissing)
	}

	if n.EndData == "" {
		return errors.New(QueryEndMissing)
	}

	if err := checkFormat(n.StartDate); err != nil {
		return err
	}
	if err := checkRange(n.StartDate); err != nil {
		return err
	}

	if err := checkFormat(n.EndData); err != nil {
		return err
	}
	if err := checkRange(n.EndData); err != nil {
		return err
	}
	if err := checkOrder(n); err != nil {
		return err
	}

	return nil
}

//checkFormat make sure the format of the passed in date is YYYY-MM-DD
func checkFormat(date string) error {
	var err error

	dateChunks := strings.Split(date, "-")

	if len(dateChunks) != 3 {
		return errors.New(FormatError)
	}

	year := dateChunks[0]
	month := dateChunks[1]
	day := dateChunks[2]

	if _, err = strconv.Atoi(year); err != nil {
		return errors.New(FormatError)
	} else if len(dateChunks[0]) != 4 {
		return errors.New(FormatError)
	}

	if _, err = strconv.Atoi(month); err != nil {
		return errors.New(FormatError)
	} else if len(dateChunks[1]) != 2 {
		return errors.New(FormatError)
	}

	if _, err = strconv.Atoi(day); err != nil {
		return errors.New(FormatError)
	} else if len(dateChunks[2]) != 2 {
		return errors.New(FormatError)
	}

	return nil
}

//checkRange validates weather the passed in date is in possible date range
func checkRange(date string) error {

	dateChunks := strings.Split(date, "-")

	year, _ := strconv.Atoi(dateChunks[0])
	month, _ := strconv.Atoi(dateChunks[1])
	day, _ := strconv.Atoi(dateChunks[2])

	if year > time.Now().Year() {
		return errors.New(OutOffRangeError)
	} else if month > 12 {
		return errors.New(OutOffRangeError)
	} else if (month == 2) && isLeap(year) && day > 29 { // Check for leap years
		return errors.New(OutOffRangeError)
	} else if month == 2 && !isLeap(year) && day > 28 {
		return errors.New(OutOffRangeError)
	} else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && day > 31 {
		return errors.New(OutOffRangeError)
	} else if (month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) && day > 30 {
		return errors.New(OutOffRangeError)
	}

	return nil
}

//checkOrder check if the order of the dates is correct and start_date is before end_date
func checkOrder(queryDate QueryDates) error {
	sd, _ := time.Parse("2006-01-02", queryDate.StartDate)
	ed, _ := time.Parse("2006-01-02", queryDate.EndData)

	if sd == ed {
		return nil
	} else if !sd.Before(ed) {
		return errors.New(WrongDateOrder)
	}

	return nil
}

func isLeap(year int) bool {
	return year%4 == 0 && (year%100 != 0 || year%400 == 0)
}
