package models

type ImageUrl string

type PictureURLSResponse struct {
	Urls []ImageUrl `json:"urls"`
}
