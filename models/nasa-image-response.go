package models

type NASAImageResponse struct {
	Url string `json:"url"`
}

type NASAErrorResponse struct {
	Error NASAErrorContent `json:"error"`
}

type NASAErrorContent struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}
